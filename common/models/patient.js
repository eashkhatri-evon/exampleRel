'use strict';

module.exports = function(Patient) {

    Patient.prototype.addUpdateAllergies = function(allergies, callback) {
        Patient.app.models.PatientAllergy.updateAll(
            {and: [{patientId: this.id}, {allergyId: {nin: allergies}}]}, 
            {status: 'deleted'}, 
            function(err, res) {
                console.log(err);
                console.log(res);
        });

        var patAllergies = [];
        allergies.map( 
            allergy => 
            patAllergies.push(this.patientAllergies.build(
                {"allergyId": allergy, "status": "active"}
            ))
        );

        this.patientAllergies.create(patAllergies, function(err, res){
            if(err) return callback(err);
            return callback(null, res);
        });  

    }

    Patient.remoteMethod('addUpdateAllergies', {
        isStatic: false,
        http: {
            verb: 'post',
            path: '/manage-allergies'
        },
        accepts: [
            { "arg": "allergies", "type": "array", "required": true }
        ],
        returns: {
            arg: 'result',
            type: 'object'
        }
    });
    
  
};
