'use strict';

var server = require('../server/server');
var ds = server.dataSources.db;
var lbTables = [
  'user',
  'Business',
  'Doctor',
  'Patient',
  'PasswordHistory',
  'UploadedFile',
  'Allergy',
  'Illness',
  'Insurance',
  'UserInsurance',
  'UserAllergy',
  'UserIllness',
  'AccessToken',
  'ACL',
  'RoleMapping',
  'Role',
  'UsersLanguage',
  'Language',
  'Designation',
  'Medication',
  'UserMedication',
  'SubscriptionPlan',
  'FacilityType'
];
ds.autoupdate(lbTables, function (er) {
  if (er) throw er;
  console.log('Loopback tables [' - lbTables - '] updated in ', ds.adapter.name);
  ds.disconnect();
});
